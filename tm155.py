#requires python hidapi

import hid
import time

def connect_to_mouse():
	for desc in hid.enumerate():
		# search for a holtek device that makes the tm-115 tick
		if desc["vendor_id"] == 0x04d9 and desc["product_id"] == 0xa116:
			# test if we have the right interface
			if desc["interface_number"] == 2:
				# and is the usage the proprietary crap
				if desc["usage"] == 0xff00 and desc["usage_page"] == 0xff00:
					device = hid.device()
					device.open_path(desc["path"])
					return device

def send_bulk_read_command(device, command, arg=0):
	command = command | 0x80
	device.send_feature_report([command,arg,0,0,0,0,0,0xFF-((command+arg)&0xFF)])
	print(device.get_feature_report(0, 9))
	print(device.read(0x40))
	print(device.read(0x40))
	
def send_bulk_write_command(device, command, arg=0, data1 = b'' , data2 = b''):
	l = len(data1)+len(data2)
	print(device.send_feature_report([command,arg,l,0,0,0,0,0xFF-((command+arg+l)&0xFF)]))
	print(device.write(data1))
	time.sleep(0.05)
	print(device.write(data2))
	
def send_command(device, command, arg1=0, arg2=0):
	device.send_feature_report([command,arg1,arg2,0,0,0,0,0xFF-((command+arg1+arg2)&0xFF)])

def send_read_command(device, command, arg1=0, arg2=0):
	send_command(device, command|0x80, arg1, arg2)
	return device.get_feature_report(0,9)
