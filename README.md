# TM155 experiments

This repository contains information on what bytes to poke where to make TM155 mice do what you want.

Most information here is thaken from Ninji's blog post series over at [https://wuffs.org/blog/mouse-adventures](https://wuffs.org/blog/mouse-adventures).
The rest was found out through trial and error.

## Possibly compatible Mice

If your mouse indentifies itself with `04d9:a116 Holtek Semiconductor, Inc. USB Gaming Mouse`, chances are the information here applys to it.

- Titanwolf Specialist (The one I have and tested on)
- Tecknet Hypertrak (The one Ninji has/had in 2018)
More: [https://wuffs.org/blog/mouse-adventures-part-2](https://wuffs.org/blog/mouse-adventures-part-2)

(Yes this an attempt to do a bit of seo to make this easier to find)

## The Python Utilitys

The tm155.py file contains some helper functions for poking around in the mouse, they are based on Ninji's examples and use the hidapi python library. The test*.py files are for experimentation purposes, try to understand roughly what they do befor running them (you probably want to modify them too).

You can dump the configuration of your mouse using the dump_config.py script, make sure you keep a copy of the default configuration around.

NOTE: All scripts have a profile_id variable that allows you to choose a mose profile that will be written to.
The default will usually be profile 2 because that's what I use for testing. The test_set_profile.py script can be used to set the currently active profile. There are 6 profiles according to the manual that came with my mouse (starting at 0)

## Magic Values

You can find a lot of magic values in Ninji's configuration utility: [https://github.com/Treeki/TM155-tools/blob/master/tm155-mac/tm155-tool-x/TM155ControlDevice.swift](https://github.com/Treeki/TM155-tools/blob/master/tm155-mac/tm155-tool-x/TM155ControlDevice.swift)
